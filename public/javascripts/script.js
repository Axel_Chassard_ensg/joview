import * as THREE from '../three.jsGithub/build/three.module.js';

const scene = new THREE.Scene();
scene.background = new THREE.Color( 0xcce0ff );
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth*0.8 / window.innerHeight*0.6, 0.1, 1000 );

scene.add( new THREE.AmbientLight( 0x666666 ) );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth*0.8, window.innerHeight*0.6);
document.body.appendChild( renderer.domElement );

const gridHelper = new THREE.GridHelper(3000,20);
scene.add(gridHelper);

const axesHelper = new THREE.AxesHelper( 200 );
scene.add( axesHelper );



const cubeHelper = new THREE.BoxGeometry( 1, 1, 1 );
cubeHelper.applyMatrix( new THREE.Matrix4().makeTranslation( 0.50, 0.50, 0.50 ) );
const meshCubeHelper = new THREE.Mesh(cubeHelper,new THREE.MeshBasicMaterial( {color: 0x000000} ));
scene.add(meshCubeHelper);

const dicoGeom = new Map();


// for (let index = 0; index<10; index++) {
for (let index = 1; index<=10; index++) {
    const geometry = new THREE.BoxGeometry( 1, 1, 1 );
    const building = new THREE.Mesh(geometry);
    
    building.applyMatrix( new THREE.Matrix4().makeTranslation( 0.50, 0.50, 0.50 ) );

    building.scale.x = Math.floor(Math.random()*5+5);
    building.scale.z = Math.floor(Math.random()*5+5);
    building.scale.y = Math.floor(Math.random()*30+30);

    building.position.x = Math.floor(Math.random()*200);
    building.position.y = building.scale.y*0.5;
    building.position.z = Math.floor(Math.random()*(-200));
    
    dicoGeom.set(index,building);
    scene.add(building);
}



camera.position.set( 0,20,40 );

document.addEventListener('keydown', event =>{
    console.log(event.key);
    if (event.key === 'ArrowDown') {
        camera.position.z += 10;
        console.log(camera.position.z);
    }
    if (event.key === 'ArrowUp') {
        camera.position.z -= 10;
    }
    if (event.key === 'ArrowLeft') {
        camera.position.x -= 10;
    }
    if (event.key === 'ArrowRight') {
        camera.position.x += 10;
    }
    if (event.key === 'm') {
        camera.position.y -= 10;
    }   
    if (event.key === 'p') {
        camera.position.y += 10;
    } 
    if (event.key === 'c') {
        generateItineary(1,2);
    }
})

const dicoSphere = new Map();

function generateItineary(name1,name2){
    const vx = dicoGeom.get(name2).position.x - dicoGeom.get(name1).position.x;
    const vz = dicoGeom.get(name2).position.z - dicoGeom.get(name1).position.z;
    const distance = Math.sqrt(Math.pow(vx,2)+Math.pow(vz,2));
    const nbPoint = distance/4;
    for (let index = 0; index < nbPoint; index++) {
        const spherePath = new THREE.Mesh(new THREE.SphereGeometry(1),new THREE.MeshLambertMaterial({color:'0x3355ff'}));
        spherePath.translate.y = 0.5;
        spherePath.position.x = dicoGeom.get(name1).position.x + index * vx/nbPoint; 
        spherePath.position.z = dicoGeom.get(name1).position.z + index * vz/nbPoint;
        scene.add(spherePath);
        dicoSphere.set(index,spherePath);
    }
}

let iterator = 0;

function animate() {
    requestAnimationFrame( animate );

    if (dicoSphere.size !== 0) {
        console.log("there");
        iterator ++;

        if (iterator > dicoSphere.size) {
            iterator = 0;
        }

        for (const [name,sphere] of dicoSphere) {
            if (sphere.name === iterator.toString()) {
                sphere.color = "0x000000";
            }
            else{
                sphere.color = "0x3355ff";
            }
        }
    }

    
    
    // controls.update();
    renderer.render( scene, camera );
}
animate();