# JOView

This project generate randomly buildings and can calculate an itinerary between two of them. 

## How to launch the app

This is a nodejs project with expressjs web server.

Open a terminal, go to "joview" and run the following command : 

    npm install
    npm start

After that, go to localhost:3000, the application would be there. 

## How to use it 

You can move the camera by using the arrow of your keyboard.

You can go up with the key 'p' and go down with the key 'm'.

You can generate an itinerary with the key 'c'. This will render a path between two buildings represent by sphere. 

